<b>DESCRIPTION</b> <br>
With this script you can download all your github repositories to your system.
<br>
<b> HOW TO RUN </b>
- clone repository
- bash ./githubCloner.sh [username github]
- Please wait to download
<br>
<b>SAMPLE</b>
bash ./githubCloner.sh amirhf1
<br>