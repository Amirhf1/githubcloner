#!/usr/bin/env bash
# Clone all github.com repositories for a specified user.
tput reset
resetColor=$(tput sgr0)
redColor=$(tput setaf 1)
greenColor=$(tput setaf 2)
if [[ $# -lt 1 || $# -gt 1 ]]; then
echo "${redColor}Usage: $0 <user_name>${resetColor}"
exit
fi
USER=$1
if [ ! -e $USER ]; then
echo "${greenColor}Creating $USER directory${resetColor}"
mkdir -p $USER
fi
cd $USER
# clone all repositories for user specifed
repos=$(curl -s https://api.github.com/users/$USER/repos?per_page=1000 |grep git_url |awk '{print $2}'| sed 's/"\(.*\)",/\1/')
while IFS= read -r repo ; do
echo "${redColor}Cloning $repo${resetColor}"
git clone $repo
done <<< $repos
